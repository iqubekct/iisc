from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.contrib import messages
from uuid import uuid1
from Registration.forms import RegistrationForm, ParticipantDetailForm
from payment.models import Transaction
from payment.views import initiate_payment
from .models import ParticipantsDetail, RegistrationDetail
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from payment.urls import *

from payment.views import send_email

def index(request):
    return render(request, 'Registration/html/index.html')


def getamount(data, deligate_category, registration_type,early,number_participant):
    data['money'] = number_participant
    if (deligate_category == 0 and registration_type == 0):
        data['money'] *= settings.DOMESTIC_DELIGATE_INDUSTRY_C
        data['currency'] = 'Rs'
    elif (deligate_category == 0 and registration_type == 1):
        data['money'] *= settings.DOMESTIC_DELIGATE_INDUSTRY_W
        data['currency'] = 'Rs'
    elif (deligate_category == 0 and registration_type == 2):
        data['money'] *= settings.DOMESTIC_DELIGATE_INDUSTRY_CW
        data['currency'] = 'Rs'
    elif (deligate_category == 1 and registration_type == 0):
        data['money'] *= settings.DOMESTIC_DELIGATE_ACADEMIA_C
        data['currency'] = 'Rs'
    elif (deligate_category == 1 and registration_type == 1):
        data['money'] *= settings.DOMESTIC_DELIGATE_ACADEMIA_W
        data['currency'] = 'Rs'
    elif (deligate_category == 1 and registration_type == 2):
        data['money'] *= settings.DOMESTIC_DELIGATE_ACADEMIA_CW
        data['currency'] = 'Rs'
    elif (deligate_category == 2 and registration_type == 0):
        data['money'] *= settings.DOMESTIC_DELIGATE_RESEARCH_SCHOLAR_PG_STUDENT_C
        data['currency'] = 'Rs'
    elif (deligate_category == 2 and registration_type == 1):
        data['money'] *= settings.DOMESTIC_DELIGATE_RESEARCH_SCHOLAR_PG_STUDENT_W
        data['currency'] = 'Rs'
    elif (deligate_category == 2 and registration_type == 2):
        data['money'] *= settings.DOMESTIC_DELIGATE_RESEARCH_SCHOLAR_PG_STUDENT_CW
        data['currency'] = 'Rs'
    elif (deligate_category == 3 and registration_type == 0 and early):
        data['money'] *= settings.INTERANTIONAL_DELIGATE_ACADEMIA_C_E
        data['currency'] = '$'
    elif (deligate_category == 3 and registration_type == 1 and early):
        data['money'] *= settings.INTERANTIONAL_DELIGATE_ACADEMIA_W_E
        data['currency'] = '$'
    elif (deligate_category == 3 and registration_type == 2 and early):
        data['money'] *= settings.INTERANTIONAL_DELIGATE_ACADEMIA_CW_E
        data['currency'] = '$'
    elif (deligate_category == 4 and registration_type == 0 or not early):
        data['money'] *= settings.INTERANTIONAL_DELIGATE_ACADEMIA_C_L
        data['currency'] = '$'
    elif (deligate_category == 4 and registration_type == 1 or not early):
        data['money'] *= settings.INTERANTIONAL_DELIGATE_ACADEMIA_W_L
        data['currency'] = '$'
    elif (deligate_category == 4 and registration_type == 2 or not early):
        data['money'] *= settings.INTERANTIONAL_DELIGATE_ACADEMIA_CW_L
        data['currency'] = '$'
    return data

def register(request):
    data = {'money': 0, 'currency': ''}
    registrationform = RegistrationForm()
    participantdetailform = ParticipantDetailForm()
    if request.method == 'POST':
        if valid(request.POST):
            organisation = request.POST['id_organisation']
            number_of_participants = request.POST['id_number_participants']
            organisation_address = request.POST['id_organisation_address']
            deligate_category = request.POST['id_deligate_category']
            registration_type = request.POST['id_registration_type']
            newReg = RegistrationDetail.objects.create(organisation=organisation,
                                                       number_participants=int(number_of_participants),
                                                       deligate_category=deligate_category,
                                                       organisation_address=organisation_address,
                                                       registration_type=registration_type)
            newReg.save()
            name = request.POST.getlist('id_name')
            email = request.POST.getlist('id_email')
            phone = request.POST.getlist('id_phone')
            gender = request.POST.getlist('id_gender')
            designation = request.POST.getlist('id_designation')
            # partticipants Detail
            for i in range(0, len(name)):
                par = ParticipantsDetail.objects.create(name=name[i], email=email[i], phone=phone[i], gender=gender[i],
                                                  designation=designation[i], registration=newReg)
                par.save()
            data = getamount(data, deligate_category=int(newReg.deligate_category),registration_type=int(newReg.registration_type),early=True,number_participant=newReg.number_participants)
            if data['currency'] == '$':
                id = Transaction.objects.create(registration=newReg, status=0, amount_d=int(data['money']), method=0,
                                                txnid=str(uuid1().int >> 64))
                id = id.txnid
            else:
                id = Transaction.objects.create(registration=newReg, status=0, amount_inr=int(data['money']), method=0,
                                                txnid=str(uuid1().int >> 64))
                id = id.txnid
                request.session["transaction_id"] = id
                return redirect("payubiz_entrance")

    return render(request, 'Registration/html/register.html',
                  {'registrationform': registrationform, 'participantdetailform': participantdetailform})


@csrf_exempt
def gettotal(request):
    early = True
    data = {'money': 0, 'currency': ''}
    number_participant = int(request.POST['number_participant'])
    deligate_category = int(request.POST['deligate_category'])
    registration_type = int(request.POST['registration_type'])
    if request.is_ajax():
        data = getamount(data, deligate_category, registration_type,early,number_participant)
    else:
        data['money'] = 0
        data['currency'] = ''
    return JsonResponse(data)


def valid(data):
    try:
        organisation = data['id_organisation']
        number_of_participants = data['id_number_participants']
        organisation_address = data['id_organisation_address']
        deligate_category = data['id_deligate_category']
        registration_type = data['id_registration_type']
        name = data.getlist('id_name')
        email = data.getlist('id_email')
        phone = data.getlist('id_phone')
        gender = data.getlist('id_gender')
        designation = data.getlist('id_designation')
    except:
        return False
    messages = ""
    if organisation == "":
        return False
    elif number_of_participants == "":
        return False
    elif organisation_address == "":
        return False
    elif deligate_category == "":
        return False
    elif registration_type == "":
        return False
    elif name == "":
        return False
    elif email == "":
        return False
    elif phone == "":
        return False
    elif gender == "":
        return False
    elif designation == "":
        return False
    else:
        return True
