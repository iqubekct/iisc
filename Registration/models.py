from django.db import models

# Create your models here.
class RegistrationDetail(models.Model):
    NO_OF_PARTICIPANTS = ((1, "1 Participant"),
                          (2, "2 Participants"),
                          (3, "3 Participants"),
                          (4, "4 Participants"),
                          (5, "5 Participants"),
                          (6, "6 Participants"),
                          (7, "7 Participants"),
                          (8, "8 Participants"),
                          (9, "9 Participants"),
                          (10, "10 Participants"))
    NO_OF_DAYS = ((1,"1 DAY"),
                  (2,"2 DAYs"))
    TYPE_CHOICE = ((0,"CONFERENCE ONLY"),
                   (1,"PRE-CONFERENCE WORKSHOP ONLY"),
                   (2,"PRE-CONFERENCE WORKSHOP & CONFERENCE PACKAGE "))
    DELIGATE_CATEGORY = ((0,"DOMESTIC DELIGATE - INDUSTRY"),
                         (1,"DOMESTIC DELIGATE - ACADEMIA"),
                         (2,"DOMESTIC DELIGATE - RESEARCH SCHOLAR/PG STUDENT"),
                         (3,"INTERANTIONAL DELIGATE - ACADEMIA"),
                         (4,"INTERANTIONAL DELIGATE - RESEARCH SCHOLAR/PG STUDENT"))
    organisation = models.CharField(max_length=1024)
    number_participants = models.IntegerField(choices=NO_OF_PARTICIPANTS)
    organisation_address = models.CharField(max_length=1024)
    is_paid = models.BooleanField(default=False)
    deligate_category = models.IntegerField(choices=DELIGATE_CATEGORY,null=True)
    created_at = models.DateTimeField(auto_now=True)
    registration_type = models.IntegerField(choices=TYPE_CHOICE,null=True)

class ParticipantsDetail(models.Model):
    GENDER = ((0, "MALE"),
              (1, "FEMALE"))
    name = models.CharField(max_length=512)
    email = models.CharField(max_length=1024)
    phone = models.CharField(max_length=10)
    designation = models.CharField(max_length=512)
    gender = models.IntegerField(choices=GENDER)
    registration = models.ForeignKey('RegistrationDetail',on_delete=models.CASCADE,null=True)
    created_at = models.DateTimeField(auto_now=True)