from django.forms import ModelForm
from Registration.models import RegistrationDetail, ParticipantsDetail


class RegistrationForm(ModelForm):
    class Meta:
        model = RegistrationDetail
        exclude = ('is_paid', 'participants', 'created_at')

    def is_valid(self):
        valid = super(RegistrationForm, self).is_valid()
        if not valid:
            return False
        data = self.cleaned_data
        organisation = data['id_organisation']
        number_participants = data['id_number_participants']
        organisation_address = data['id_organisation_address']
        deligate_category = data['id_deligate_category']
        registration_type = data['id_registration_type']

        if organisation == "":
            valid = False
            self._errors['id_organisation'] = [u'organisation should not be empty']
        elif number_participants == "":
            valid = False
            self._errors['id_number_participants'] = [u'select number of participants']
        elif organisation_address == "":
            valid = False
            self._errors['id_organisation_address'] = [u'organisation address should not be empty']
        elif deligate_category == "":
            valid = False
            self._errors['id_deligate_category'] = [u'select deligate category']
        elif registration_type == "":
            valid = False
            self._errors['id_registration_type'] = [u'select registration_type']


class ParticipantDetailForm(ModelForm):
    class Meta:
        model = ParticipantsDetail
        fields = ['name', 'email', 'phone', 'designation', 'gender']

    def is_valid(self):
        valid = super(ParticipantDetailForm, self).is_valid()
        if not valid:
            return False
        data = self.cleaned_data
        name = data['id_name']
        email = data['id_email']
        phone = data['phone']
        designation = data['designation']
        gender = data['gender']
        if name == "":
            valid = False
            self._errors['id_name'] = [u'select registration_type']
        elif email == "":
            valid = False
            self._errors['id_email'] = [u'select registration_type']
        elif phone == "":
            valid = False
            self._errors['id_phone'] = [u'select registration_type']
        elif designation == "":
            valid = False
            self._errors['id_email'] = [u'select registration_type']
        elif gender == "":
            valid = False
            self._errors['id_gender'] = [u'select registration_type']