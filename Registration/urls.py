from django.conf.urls import url
from django.conf import settings

from .views import index,register,gettotal

urlpatterns = [
    url(r'^$',index,name="index"),
    url(r'^registration/',register,name="register"),
    url(r'^gettotal/',gettotal,name="gettotal"),
]
