from django.db import models
from Registration.models import RegistrationDetail
# Create your models here.
class Transaction(models.Model):
    TYPE = ((0,"PAYUBIZ"),
            (1,"PAYPAL"))
    STATUS_CHOICE = ((0,"Initiated"),
                     (1,"Paid"),
                     (2,"Cancled"),
                     (3,"Failed"))
    created_at = models.DateTimeField(auto_now=True)
    amount_inr = models.CharField(max_length=1024,default=0)
    amount_d = models.CharField(max_length=1024,default=0)
    method = models.IntegerField(choices=TYPE)
    status = models.IntegerField(choices=STATUS_CHOICE)
    registration = models.ForeignKey('Registration.RegistrationDetail',on_delete=models.CASCADE)
    txnid=models.CharField(max_length=20,null=True)

    error_code = models.CharField(max_length=255, null=True)
    error_message = models.CharField(max_length=255, null=True)
    bank_refnum = models.CharField(max_length=255, null=True)
    refund_amount = models.IntegerField(default=0, null=True)
    additional_charges = models.FloatField(default=0, null=True)
    field9 = models.TextField(max_length=512, null=True)
    payment_added_on = models.DateTimeField(null=True)
    pg_type = models.CharField(max_length=255, null=True)
    payment_id = models.CharField(max_length=10, null=True)
    payu_status = models.BooleanField(default=0)
    mihpay_id = models.CharField(max_length=30, null=True)  # don't know the use