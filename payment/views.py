# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django.contrib.auth.decorators import login_required
import requests
from django.core.exceptions import SuspiciousOperation
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from payment.util import generate_hash, verify_hash
from .models import Transaction
from django.contrib import messages
from Registration.models import *
from django.template import loader
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import send_mail
from django.conf import settings

import os

# Create your views here.



def initiate_payment(request,txnid):
    transaction = Transaction.objects.get(txnid)
    if(transaction.amount_d != 0):
        pass
    elif(transaction.amount_inr != 0):
        pass

def payment_payubiz(request):
    if request.method == "POST" or "GET":
        txnid=request.session.get("transaction_id")
        transaction = Transaction.objects.get(txnid=txnid)
        title = "Enrollment"
        amount = int(transaction.amount_inr)

        # email = transaction.registration__participantsdetail[0].email
        email = ParticipantsDetail.objects.filter(registration=transaction.registration)
        mkey = settings.PAYU_INFO['merchant_key']
        surl = request.build_absolute_uri(reverse('succ_paybiz'))
        curl = request.build_absolute_uri(reverse('cancel_payubiz'))
        furl = request.build_absolute_uri(reverse('failure_payubiz'))

        data = dict(key=mkey, txnid=txnid, amount=amount, productinfo=title, firstname="arun", email=email,phone="8249076464")
        hash = generate_hash(data)
        data.update({'hash': hash, 'surl': surl, 'curl': curl, 'furl': furl})
        print(data)
        return render(request, 'payment/pay_redirect.html', {'form': data})
    else:
        raise SuspiciousOperation("Invalid request")


def key_collect_payubiz(request):
    data = {'key': request.POST.get('key'), 'txnid': request.POST.get('txnid'), 'amount': request.POST.get('amount'),
            'productinfo': request.POST.get('productinfo'), 'firstname': request.POST.get('firstname'),
            'email': request.POST.get('email'), 'hash': request.POST.get('hash'), 'status': request.POST.get('status')}
    try:
        data.update({'additionalCharges': request.POST.get('additionalCharges')})
    except:
        pass
    return data


@csrf_exempt
def succ_payubiz(request):
    msg=""
    if request.method == "POST":
        data = key_collect_payubiz(request)

        if verify_hash(data):
            trns = Transaction.objects.get(txnid=data['txnid'])
            trns.payment_id = request.POST.get('payuMoneyId')
            trns.payu_status = (request.POST.get('status') == 'success')  # Boolean
            trns.status = 1
            trns.error_code = request.POST.get('error')  # should return no_error e000
            trns.error_message = request.POST.get('error_Message')
            trns.bank_refnum = request.POST.get('bank_ref_num')
            trns.mihpay_id = request.POST.get('mihpayid')
            trns.payment_added_on = request.POST.get('addedon')
            trns.pg_type = request.POST.get('PG_TYPE')
            trns.payment_mode = request.POST.get('mode')
            trns.additional_charges = data.get("additionalCharges", 0)
            trns.field9 = request.POST.get('field9')
            trns.save()
            reg = RegistrationDetail.objects.get(id=trns.registration.id)
            reg.is_paid = True
            reg.save()
            msg = "Payment Success"
            email_list = [i.email for i in ParticipantsDetail.objects.filter(registration=reg.id)]
            send_email(email_list)
    else:
        raise SuspiciousOperation("Invalid access")
    messages.success(request,msg)
    return redirect('paysuccess')




@csrf_exempt
def failure_payubiz(request):
    if request.method == "POST":
        data = key_collect_payubiz(request)
        if verify_hash(data):
            trns = Transaction.objects.get(txnid=data['txnid'])
            trns.payment_id = request.POST.get('payuMoneyId')
            trns.payu_status = (request.POST.get('status') == 'success')  # Boolean
            trns.status = 3
            trns.error_code = request.POST.get('error')  # should return no_error e000
            trns.error_message = request.POST.get('error_Message')
            trns.bank_refnum = request.POST.get('bank_ref_num')
            trns.mihpay_id = request.POST.get('mihpayid')
            trns.payment_added_on = request.POST.get('addedon')
            trns.pg_type = request.POST.get('PG_TYPE')
            trns.payment_mode = request.POST.get('mode')
            trns.additional_charges = data.get("additionalCharges", 0)
            trns.field9 = request.POST.get('field9')
            trns.save()
            return redirect('payfailure')
        else:
            pass
    else:
        raise SuspiciousOperation("Invalid access")
    pass

@csrf_exempt
def cancel_payubiz(request):
    if request.method == "POST":
        data = key_collect_payubiz(request)
        if verify_hash(data):
            trns = Transaction.objects.get(txnid=data['txnid'])
            trns.payment_id = request.POST.get('payuMoneyId')
            trns.payu_status = (request.POST.get('status') == 'success')  # Boolean
            trns.status = 2
            trns.error_code = request.POST.get('error')  # should return no_error e000
            trns.error_message = request.POST.get('error_Message')
            trns.bank_refnum = request.POST.get('bank_ref_num')
            trns.mihpay_id = request.POST.get('mihpayid')
            trns.payment_added_on = request.POST.get('addedon')
            trns.pg_type = request.POST.get('PG_TYPE')
            trns.payment_mode = request.POST.get('mode')
            trns.additional_charges = data.get("additionalCharges", 0)
            trns.field9 = request.POST.get('field9')
            trns.save()
            reurl = reverse('index', args=(), kwargs={})
            return HttpResponseRedirect(reurl)
        else:
           pass
    else:
        raise SuspiciousOperation("Invalid access")
    pass

def payment_paypal(request,txnid):
    pass

def send_email(to_mail):
    file_path = os.path.join(settings.BASE_DIR, "templates\\Payment\\html\\success_email.html")
    html = open(file_path,"rb").read()
    return requests.post(
        "https://api.mailgun.net/v3/mail.iqube.io/messages",
        auth=("api", 'key-26d235b4d4398d28912ff2fbb56bf78d'),
        data={"from": "no-reply-kct@kct.ac.in",
              "to": to_mail,
              "subject": "Indo - Israel International Joint Conference on Sustainable Cities - Registration Successful",
              "text": "Testing some Mailgun awesomness!",
              "html":html},
    )

