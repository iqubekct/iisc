from hashlib import sha512
from django.conf import settings

KEYS = ('txnid', 'amount', 'productinfo', 'firstname', 'email',
        'udf1', 'udf2', 'udf3', 'udf4', 'udf5', 'udf6', 'udf7', 'udf8',
        'udf9', 'udf10')


def generate_hash(data, ):
#     hash_value = sha512(str(getattr(settings, 'PAYU_MERCHANT_KEY', None)).encode('utf-8'))
#
#     for key in KEYS:
#             hash_value.update(("%s%s" % ('|', str(data.get(key, '')))).encode("utf-8"))
#             print(data.get(key,""))
#
#     hash_value.update(("%s%s".format('|', getattr(settings, 'PAYU_MERCHANT_SALT', None))).encode('utf-8'))

    # Create transaction record
    key=settings.PAYU_MERCHANT_KEY
    salt=settings.PAYU_MERCHANT_SALT
    txnid=data["txnid"]
    amount=data['amount']
    productinfo=data['productinfo']
    firstname=data['firstname']
    email=data["email"]
    print(txnid)
    hash_value=sha512(str("{}|{}|{}|{}|{}|{}|||||||||||{}").format(key,txnid,amount,productinfo,firstname,email,salt).encode("utf-8"))
    return hash_value.hexdigest().lower()


KEYS_MOBILE = ('txnid', 'amount', 'productinfo', 'firstname', 'email',
               'udf1', 'udf2', 'udf3', 'udf4', 'udf5')


def generate_hash_mobile(data):
    hash_value = sha512(settings.PAYU_INFO.get('merchant_key'))
    for key in KEYS_MOBILE:
        hash_value.update("%s%s" % ('|', data.get(key, '')))

    hash_value.update("%s%s" % ('|', settings.PAYU_INFO.get('merchant_salt')))

    return hash_value.hexdigest().lower()


def verify_hash(data):
    Reversedkeys = reversed(KEYS)
    if data.get('additionalCharges'):

        hash_value = sha512(data.get('additionalCharges'))
        hash_value.update("%s%s" % ('|', settings.PAYU_INFO.get('merchant_salt').encode("utf-8")))
    else:

        hash_value = sha512(settings.PAYU_INFO.get('merchant_salt').encode("utf-8"))

    hash_value.update(("%s%s" % ('|', str(data.get('status', '')))).encode('utf-8'))

    for key in Reversedkeys:
        hash_value.update(("%s%s" % ('|', str(data.get(key, '')))).encode('utf-8'))

    hash_value.update(("%s%s" % ('|', settings.PAYU_INFO.get('merchant_key'))).encode('utf-8'))

    return hash_value.hexdigest().lower() == data.get('hash')
