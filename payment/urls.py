from django.conf.urls import url
from django.views.generic import TemplateView

from . import views



urlpatterns = [
    url(r'^payubiz_success/$', views.succ_payubiz, name="succ_paybiz"),
    url(r'^failure_payubiz/$', views.failure_payubiz, name="failure_payubiz"),
    url(r'^cancel_payubiz/$', views.cancel_payubiz, name="cancel_payubiz"),
    url(r'^payubiz_entrance/$', views.payment_payubiz, name="payubiz_entrance"),
    url(r'^paymentsuccess/$', TemplateView.as_view(template_name="payment/success.html"), name='paysuccess'),
    url(r'^paymentfailure/$', TemplateView.as_view(template_name="payment/failure.html"), name='payfailure'),
    ]